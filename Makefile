CC=gcc
FLAGS=-I./src/
TARGETS=analysis

main: $(TARGETS)


lib/%.o: src/%.c
	$(CC) $(FLAGS) -c $< -o $@

clean:
	rm -f $(TARGETS) lib/*.o

%:lib/key.o lib/%.o
	$(CC) -o $@ $^
