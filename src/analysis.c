/*
 * analysis.c
 *
 *  Created on: Aug 11, 2014
 *      Author: Dom
 */
#include <string.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "key.h"
//#include "globalDefines.h"
#include "analysis.h"

songAnalysis analyseSong(key keyOfSong, noteEvent* events,int sizeEvents){
	songAnalysis song;
	song.songKey = keyOfSong;
	int measure = events[0].measure;
	int i = 0;
	while(i<sizeEvents){
		int root=0,second=0,third=0,fourth=0,fifth=0,sixth=0,seventh=0;
		note strong[4];
		while(measure == events[i].measure && i<sizeEvents){
			if(events[i].beat == 1.0){
				strong[0] = events[i].notePlayed;
			}
			else if(events[i].beat == 1.0){
				strong[1] = events[i].notePlayed;
			}


			if (events[i].notePlayed.id ==  keyOfSong.notes[0].id){
				root++;
			}
			else if (events[i].notePlayed.id ==  keyOfSong.notes[1].id){
				second++;
			}

			else if (events[i].notePlayed.id ==  keyOfSong.notes[2].id){
				third++;
			}

			else if (events[i].notePlayed.id ==  keyOfSong.notes[3].id){
				fourth++;
			}

			else if (events[i].notePlayed.id ==  keyOfSong.notes[4].id){
				fifth++;
			}

			else if (events[i].notePlayed.id ==  keyOfSong.notes[5].id){
				sixth++;
			}

			else if (events[i].notePlayed.id ==  keyOfSong.notes[5].id){
				seventh++;
			}
		
		i++;
		}
		measure++;
		printf("1:%d\t2:%d\t3:%d\t4:%d\t5:%d\t6:%d\t7:%d\t\n",root,second,third,fourth,fifth,sixth,seventh);
	}
	return song;
	
} 

int main(void){
	note Ab_NOTE = {"Ab",G_SHARP,A,FLAT};
	note A_NOTE = {"A",A_NATURAL,A,NATURAL};
	note AS_NOTE = {"A#",B_FLAT,A,SHARP};

	note Bb_NOTE = {"Bb",B_FLAT,B,FLAT};
	note B_NOTE = {"B",B_NATURAL,B,NATURAL};
	note BS_NOTE = {"B#",C_NATURAL,B,SHARP};

	note Cb_NOTE = {"Cb",B_NATURAL,C,FLAT};
	note C_NOTE = {"C",C_NATURAL,C,NATURAL};
	note CS_NOTE = {"C#",C_SHARP,C,SHARP};

	note Db_NOTE = {"Db",C_SHARP,D,FLAT};
	note D_NOTE = {"D",D_NATURAL,D,NATURAL};
	note DS_NOTE = {"D#",D_SHARP,D,SHARP};

	note Eb_NOTE = {"Eb",D_SHARP,E,FLAT};
	note E_NOTE = {"E",E_NATURAL,E,NATURAL};
	note ES_NOTE = {"E#",F_NATURAL,E,SHARP};

	note Fb_NOTE = {"Fb",E_NATURAL,F,FLAT};
	note F_NOTE = {"F",F_NATURAL,F,NATURAL};
	note FS_NOTE = {"F#",F_SHARP,F,SHARP};

	note Gb_NOTE = {"Gb",F_SHARP,G,FLAT};
	note G_NOTE = {"G",G_NATURAL,G,NATURAL};
	note GS_NOTE = {"G#",G_SHARP,G,SHARP};


	noteEvent note_1 = {C_NOTE,1,1.0};
	noteEvent note_2 = {D_NOTE,1,2.0};
	noteEvent note_3 = {E_NOTE,1,3.0};

	noteEvent note_4 = {F_NOTE,2,1.0};
	noteEvent note_5 = {C_NOTE,2,2.0};
	noteEvent note_6 = {B_NOTE,2,2.5};
	noteEvent note_7 = {A_NOTE,2,3.0};
	noteEvent note_8 = {C_NOTE,1,4.0};
	
	noteEvent note_9 = {D_NOTE,1,1.0};	
	noteEvent note_10 = {E_NOTE,1,2.0};
	noteEvent note_11 = {F_NOTE,1,3.0};

	noteEvent note_12 = {G_NOTE,1,1.0};
	noteEvent note_13 = {B_NOTE,1,2.0};
	noteEvent note_14 = {C_NOTE,1,2.5};
	noteEvent note_15 = {B_NOTE,1,3.0};
		
	noteEvent arr[3] = {note_1,note_2,note_3};
	
	key this = getKeyInfo(C_MAJOR);	
	analyseSong(this,arr,3);
	return 0;

}
