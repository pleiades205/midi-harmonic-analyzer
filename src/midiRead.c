#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>

enum PITCH_NAME {
	C, D, E, F, G, A, B
};

typedef struct {
	int pitch;
	int letter;
	int accidental;
	int start;  // beats from start of piece
	int end;    // duration (beats from start)
	int channel;
	int track;
} Event;

typedef struct {
	int tSig_numerator;
	int tSig_denominator;
	int tSig_click;
	int tSig_NotesQ;
	int kSig_key; // +1 for each sharp, -1 for each flat
	int kSig_Mm;  // 1 for major, 0 for minor
	int numberTracks;
	int format;
	int division;
	int tempo;
} PieceInfo;

void resizeArray(Event*** notes, int *arrSize, int numNotes) {
	if (numNotes >= *arrSize) {
		*arrSize = *arrSize * 2;
		*notes = (Event**) realloc(*notes, *arrSize * sizeof(Event*));
	}
}

int readMidi(FILE* f, Event*** notes, PieceInfo** piece, int* total) {
	int arrSize = 1;  // array size
	int i = 0;        // index
	int val1, val2, val3, val4, val5; // integers to store midi data
	char line[100];   // buffer to store each line from file
	char * pch;       // pointer to store each value from line

	*total = 0;

	// allocate piece struct and array of notes
	*piece = (PieceInfo*) malloc(sizeof(PieceInfo));
	*notes = (Event**) malloc(arrSize * sizeof(Event*));
	
	// parse file one line at a time
	while(!feof(f)) {
		if (fgets(line, 100, f) != NULL) {
			// get the first two integers
			pch = strtok(line, ", ");
			val1 = atoi(pch);
			pch = strtok(NULL, ", ");
			val2 = atoi(pch);
			pch = strtok(NULL, ", ");

			// Check the third value in the line
			if (strcmp(pch, "Note_on_c") == 0) {
				// get the remaining values
				pch = strtok(NULL, ", ");
				val3 = atoi(pch);
				pch = strtok(NULL, ", ");
				val4 = atoi(pch);
				pch = strtok(NULL, ", ");
				val5 = atoi(pch);

				// if this is actually a note-off event...
				if (val5 == 0) {
					// Search for the correct struct and set off time
					for (i = 0; i < *total; i++) {
						if (val1 == (*notes)[i]->track &&
						    val3 == (*notes)[i]->channel &&
						    val4 == (*notes)[i]->pitch &&
						    (*notes)[i]->end == -1) {
							(*notes)[i]->end = val2 - (*notes)[i]->start;
						break;
						}
					}
				}
				else {  // else, allocate new note and set fields
					resizeArray(notes, &arrSize, *total);
					(*notes)[*total] = (Event*) malloc(sizeof(Event));
					(*notes)[*total]->pitch = val4;
					(*notes)[*total]->start = val2;
					(*notes)[*total]->end = -1;
					(*notes)[*total]->channel = val3;
					(*notes)[*total]->track = val1;
					(*total)++;
				}
			}
			else if (strcmp(pch, "Note_off_c") == 0) {
				pch = strtok(NULL, ", ");
				val3 = atoi(pch);
				pch = strtok(NULL, ", ");
				val4 = atoi(pch);
				for (i = 0; i < *total; i++) {
					if (val1 == (*notes)[i]->track &&
					    val3 == (*notes)[i]->channel &&
					    val4 == (*notes)[i]->pitch &&
					    (*notes)[i]->end == -1) {
						(*notes)[i]->end = val2 - (*notes)[i]->start;
						break;
					}
				}
			}
			else if (strcmp(pch, "Time_signature") == 0) {
				pch = strtok(NULL, ", ");
				(*piece)->tSig_numerator = atoi(pch);
				pch = strtok(NULL, ", ");
				(*piece)->tSig_denominator = atoi(pch);
				pch = strtok(NULL, ", ");
				(*piece)->tSig_click = atoi(pch);
				pch = strtok(NULL, ", ");
				(*piece)->tSig_NotesQ = atoi(pch);
			}
			else if(strcmp(pch, "Key_signature") == 0) {
				pch = strtok(NULL, ", ");
				(*piece)->kSig_key = atoi(pch);
				pch = strtok(NULL, ", ");
				if (strncmp(pch, "\"major\"", 7) == 0) {
					(*piece)->kSig_Mm = 1;
				}
				else {
					(*piece)->kSig_Mm = 0;
				}
			}
			else if (strcmp(pch, "Tempo") == 0) {
				pch = strtok(NULL, ", ");
				(*piece)->tempo = atoi(pch);
			}
			else if (strcmp(pch, "Header") == 0) {
				pch = strtok(NULL, ", ");
				(*piece)->format = atoi(pch);
				pch = strtok(NULL, ", ");
				(*piece)->numberTracks = atoi(pch);
				pch = strtok(NULL, ", ");
				(*piece)->division = atoi(pch);
			}
		}
	}
	return 0;
}

int main(int argc, char** argv) {
	FILE* in;          // input midi file
	Event ** notes;    // array of notes
	PieceInfo * piece; // stores general piece info
	int numNotes, i;   // number of notes, index
	pid_t child, wpid; // child process, waiting process
	int status;        // child status
	int fd;            // descriptor for temporary file

	// Fork a process
	if ((child = fork()) < 0) {
		perror("Could not fork process");
	}

	// child runs midicsv on argv[0], and redirects the
	// output to a file called temp_midi.txt
	if (child == 0) {
		if ((fd = open("temp_midi.txt", O_RDWR | O_CREAT, S_IRUSR | S_IWUSR)) < 0) {
			perror("open");
		}
		if (dup2(fd, 1) < 0) {
			perror("dup2");
		}
		if (close(fd) < 0) {
			perror("close");
		}
		execve("./midicsv", argv, 0);
	}
	// parent waits for child to finish
	else if ((wpid = waitpid(child, &status, 0)) < 0) {
		perror("waitpid error");
	}

	// open text generatated by MIDICSV program
	if ( (in = fopen("temp_midi.txt", "r")) == NULL) {
		perror("fopen");
	}

	// read in midi data and store to array of structs
	if (readMidi(in, &notes, &piece, &numNotes) != 0) {
		perror("readMidi");
	}

	// fork a process to remove temp_midi.txt
	if ((child = fork()) < 0) {
		perror("Could not fork process");
	}

	// child removes temp_midi.txt
	if (child == 0) {
		execl("/bin/rm", "/bin/rm", "temp_midi.txt", NULL);
		printf("OOPS\n");
	}
	// parent waits for child to finish
	else if ((wpid = waitpid(child, &status, 0)) < 0) {
		perror("waitpid error");
	}

	///////////////////////////////
	///////////////////////////////
	// Analysis Goes Here
	///////////////////////////////
	///////////////////////////////

	// test: print out the data
	for (i = 0; i < numNotes; i++) {
		printf("track: %d  start: %d  end: %d  channel: %d  pitch: %d\n", 
			notes[i]->track, notes[i]->start, notes[i]->end, notes[i]->channel, notes[i]->pitch );
	}
	printf("Header info: %d %d %d\n", piece->format, piece->numberTracks, piece->division);
	printf("Tempo: %d\n", piece->tempo);
	printf("TimeSig info: %d %d %d %d\n", piece->tSig_numerator, piece->tSig_denominator,
		piece->tSig_click, piece->tSig_NotesQ);
	printf("KeySig info: %d %d\n", piece->kSig_key, piece->kSig_Mm);


	// close file, free memory
	if (fclose(in) != 0) {
		perror("fclose");
	}
	for(i = 0; i < numNotes; i++) {
		free(notes[i]);
	}
	free(notes);
	free(piece);
}
