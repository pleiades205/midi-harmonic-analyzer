#ifndef GLOBALDEFINES_H_INCLUDED
#define GLOBALDEFINES_H_INCLUDED


#include "key.h"


note Ab_NOTE = {"Ab",G_SHARP,A,FLAT};
note A_NOTE = {"A",A_NATURAL,A,NATURAL};
note AS_NOTE = {"A#",B_FLAT,A,SHARP};

note Bb_NOTE = {"Bb",B_FLAT,B,FLAT};
note B_NOTE = {"B",B_NATURAL,B,NATURAL};
note BS_NOTE = {"B#",C_NATURAL,B,SHARP};

note Cb_NOTE = {"Cb",B_NATURAL,C,FLAT};
note C_NOTE = {"C",C_NATURAL,C,NATURAL};
note CS_NOTE = {"C#",C_SHARP,C,SHARP};

note Db_NOTE = {"Db",C_SHARP,D,FLAT};
note D_NOTE = {"D",D_NATURAL,D,NATURAL};
note DS_NOTE = {"D#",D_SHARP,D,SHARP};

note Eb_NOTE = {"Eb",D_SHARP,E,FLAT};
note E_NOTE = {"E",E_NATURAL,E,NATURAL};
note ES_NOTE = {"E#",F_NATURAL,E,SHARP};

note Fb_NOTE = {"Fb",E_NATURAL,F,FLAT};
note F_NOTE = {"F",F_NATURAL,F,NATURAL};
note FS_NOTE = {"F#",F_SHARP,F,SHARP};

note Gb_NOTE = {"Gb",F_SHARP,G,FLAT};
note G_NOTE = {"G",G_NATURAL,G,NATURAL};
note GS_NOTE = {"G#",G_SHARP,G,SHARP};


key Cb_MAJOR_KEY= {"Cb Major", Cb_MAJOR,};
key Gb_MAJOR_KEY= {"Gb Major", Gb_MAJOR,};
key Db_MAJOR_KEY= {"Db Major", Db_MAJOR,};
key Ab_MAJOR_KEY= {"Ab Major", Ab_MAJOR,};
key Eb_MAJOR_KEY= {"Eb Major", Eb_MAJOR,};
key Bb_MAJOR_KEY= {"Bb Major", Bb_MAJOR,};
key F_MAJOR_KEY= {"F Major", F_MAJOR,};
key C_MAJOR_KEY= {"C Major", C_MAJOR,};
key G_MAJOR_KEY= {"G Major", G_MAJOR,};
key D_MAJOR_KEY= {"D Major", D_MAJOR,};
key A_MAJOR_KEY= {"A Major", A_MAJOR,};
key E_MAJOR_KEY= {"E Major", E_MAJOR,};
key B_MAJOR_KEY= {"B Major", B_MAJOR,};
key FS_MAJOR_KEY= {"F# Major", FS_MAJOR,};
key CS_MAJOR_KEY= {"C# Major", CS_MAJOR,};

#endif // GLOBALDEFINES_H_INCLUDED
