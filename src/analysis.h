#ifndef ANALYSIS_H_INCLUDED
#define ANALYSIS_H_INCLUDED


#include "key.h"
//#include "globalDefines.h"


typedef enum{
I=0,
II=1,
III=2,
IV=3,
V=4,
VI=5,
VII=6}romanNumeralSymbol; 

typedef struct romanNumeral romanNumeral;
struct romanNumeral{
	char * name;
	romanNumeralSymbol sym;
	int measure;
	double beat;
	
};
typedef struct songAnalysis songAnalysis;
struct songAnalysis{
	key songKey;
	romanNumeral * analysis;

};

typedef struct noteEvent noteEvent;
struct noteEvent{

	note notePlayed;
	int measure;
	double beat;
};

songAnalysis analyseSong(key keyOfSong, noteEvent* events,int sizeEvents);

#endif // ANALYSIS_H_INCLUDED

