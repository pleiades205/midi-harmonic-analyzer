/*
 * key.h
 *
 *  Created on: Aug 8, 2014
 *      Author: Dom
 */
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "key.h"


int main(void){
	
	int i,j;
	for(i=Cb_MAJOR;i<=CS_MAJOR;i++){
		key my_key = getKeyInfo(i);
		printf("Key of %10s:\t",my_key.name);
		for(j=0;j<NUMBER_OF_NOTE_LETTERS;j++){
			printf("%s\t", my_key.notes[j].name);
		}
		printf("\n");
	}
	return 0;
}
