#ifndef KEY_H_INCLUDED
#define KEY_H_INCLUDED

/*
 * key.h
 *
 *  Created on: Aug 8, 2014
 *      Author: Dom
 */

#include <string.h>
#include <math.h>
#include <dirent.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>


typedef enum {
Cb_MAJOR = -7, 
Gb_MAJOR = -6, 
Db_MAJOR = -5, 
Ab_MAJOR = -4, 
Eb_MAJOR = -3, 
Bb_MAJOR = -2, 
F_MAJOR = -1, 
C_MAJOR = 0, 
G_MAJOR = 1, 
D_MAJOR = 2, 
A_MAJOR = 3, 
E_MAJOR = 4, 
B_MAJOR = 5,
FS_MAJOR = 6,
CS_MAJOR = 7} keyID;

typedef enum{
A = 0,
B = 1,
C = 2,
D = 3,
E = 4,
F = 5,
G = 6,
	NUMBER_OF_NOTE_LETTERS = 7} noteLetter;

typedef enum{
FLAT = -1,
NATURAL = 0,
SHARP = 1,} noteQuality;

typedef enum{
A_NATURAL = 0,
B_FLAT = 1,
B_NATURAL = 2,
C_NATURAL = 3,
C_SHARP = 4,
D_NATURAL = 5,
D_SHARP = 6,
E_NATURAL = 7,
F_NATURAL = 8,
F_SHARP = 9,
G_NATURAL = 10,
G_SHARP = 11,
NUMBER_OF_NOTE_IDS = 12}noteID;

typedef struct note note; 
struct note {
	char * name;
	noteID	id; 
	noteLetter letter;
	noteQuality quality;
	
};
typedef struct key key;
struct key {
	char* name;
	keyID id; 
	note notes[7];
};

key getKeyInfo(keyID givinKey);

#endif // KEY_H_INCLUDED

