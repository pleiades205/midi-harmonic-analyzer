/*
 * key.c
 *
 *  Created on: Aug 8, 2014
 *      Author: Dom
 */
#include <string.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "key.h"
//#include "globalDefines.h"

/*
note Ab_NOTE = {"Ab",G_SHARP,A,FLAT};
note A_NOTE = {"A",A_NATURAL,A,NATURAL};
note AS_NOTE = {"A#",B_FLAT,A,SHARP};

note Bb_NOTE = {"Bb",B_FLAT,B,FLAT};
note B_NOTE = {"B",B_NATURAL,B,NATURAL};
note BS_NOTE = {"B#",C_NATURAL,B,SHARP};

note Cb_NOTE = {"Cb",B_NATURAL,C,FLAT};
note C_NOTE = {"C",C_NATURAL,C,NATURAL};
note CS_NOTE = {"C#",C_SHARP,C,SHARP};

note Db_NOTE = {"Db",C_SHARP,D,FLAT};
note D_NOTE = {"D",D_NATURAL,D,NATURAL};
note DS_NOTE = {"D#",D_SHARP,D,SHARP};

note Eb_NOTE = {"Eb",D_SHARP,E,FLAT};
note E_NOTE = {"E",E_NATURAL,E,NATURAL};
note ES_NOTE = {"E#",F_NATURAL,E,SHARP};

note Fb_NOTE = {"Fb",E_NATURAL,F,FLAT};
note F_NOTE = {"F",F_NATURAL,F,NATURAL};
note FS_NOTE = {"F#",F_SHARP,F,SHARP};

note Gb_NOTE = {"Gb",F_SHARP,G,FLAT};
note G_NOTE = {"G",G_NATURAL,G,NATURAL};
note GS_NOTE = {"G#",G_SHARP,G,SHARP};
*/

key Cb_MAJOR_KEY= {"Cb Major", Cb_MAJOR,};
key Gb_MAJOR_KEY= {"Gb Major", Gb_MAJOR,};
key Db_MAJOR_KEY= {"Db Major", Db_MAJOR,};
key Ab_MAJOR_KEY= {"Ab Major", Ab_MAJOR,};
key Eb_MAJOR_KEY= {"Eb Major", Eb_MAJOR,};
key Bb_MAJOR_KEY= {"Bb Major", Bb_MAJOR,};
key F_MAJOR_KEY= {"F Major", F_MAJOR,};
key C_MAJOR_KEY= {"C Major", C_MAJOR,};
key G_MAJOR_KEY= {"G Major", G_MAJOR,};
key D_MAJOR_KEY= {"D Major", D_MAJOR,};
key A_MAJOR_KEY= {"A Major", A_MAJOR,};
key E_MAJOR_KEY= {"E Major", E_MAJOR,};
key B_MAJOR_KEY= {"B Major", B_MAJOR,};
key FS_MAJOR_KEY= {"F# Major", FS_MAJOR,};
key CS_MAJOR_KEY= {"C# Major", CS_MAJOR,};



key getKeyInfo(keyID givinKey){
	key ret; //return key
	
	note Ab_NOTE = {"Ab",G_SHARP,A,FLAT};
	note A_NOTE = {"A",A_NATURAL,A,NATURAL};
	note AS_NOTE = {"A#",B_FLAT,A,SHARP};

	note Bb_NOTE = {"Bb",B_FLAT,B,FLAT};
	note B_NOTE = {"B",B_NATURAL,B,NATURAL};
	note BS_NOTE = {"B#",C_NATURAL,B,SHARP};

	note Cb_NOTE = {"Cb",B_NATURAL,C,FLAT};
	note C_NOTE = {"C",C_NATURAL,C,NATURAL};
	note CS_NOTE = {"C#",C_SHARP,C,SHARP};

	note Db_NOTE = {"Db",C_SHARP,D,FLAT};
	note D_NOTE = {"D",D_NATURAL,D,NATURAL};
	note DS_NOTE = {"D#",D_SHARP,D,SHARP};

	note Eb_NOTE = {"Eb",D_SHARP,E,FLAT};
	note E_NOTE = {"E",E_NATURAL,E,NATURAL};
	note ES_NOTE = {"E#",F_NATURAL,E,SHARP};

	note Fb_NOTE = {"Fb",E_NATURAL,F,FLAT};
	note F_NOTE = {"F",F_NATURAL,F,NATURAL};
	note FS_NOTE = {"F#",F_SHARP,F,SHARP};

	note Gb_NOTE = {"Gb",F_SHARP,G,FLAT};
	note G_NOTE = {"G",G_NATURAL,G,NATURAL};
	note GS_NOTE = {"G#",G_SHARP,G,SHARP};

	switch(givinKey){
		case Cb_MAJOR:
			ret = Cb_MAJOR_KEY;
			ret.notes[0] = Cb_NOTE;	
			ret.notes[1] = Db_NOTE;	
			ret.notes[2] = Eb_NOTE;	
			ret.notes[3] = Fb_NOTE;	
			ret.notes[4] = Gb_NOTE;	
			ret.notes[5] = Ab_NOTE;	
			ret.notes[6] = Bb_NOTE;
			break;
		case Gb_MAJOR:
			ret = Gb_MAJOR_KEY;
			ret.notes[0] = Gb_NOTE;	
			ret.notes[1] = Ab_NOTE;	
			ret.notes[2] = Bb_NOTE;	
			ret.notes[3] = Cb_NOTE;	
			ret.notes[4] = Db_NOTE;	
			ret.notes[5] = Eb_NOTE;	
			ret.notes[6] = F_NOTE;
			break;
		case Db_MAJOR:
			ret = Db_MAJOR_KEY;
			ret.notes[0] = Db_NOTE;	
			ret.notes[1] = Eb_NOTE;	
			ret.notes[2] = F_NOTE;	
			ret.notes[3] = Gb_NOTE;	
			ret.notes[4] = Ab_NOTE;	
			ret.notes[5] = Bb_NOTE;	
			ret.notes[6] = C_NOTE;
			break;
		case Ab_MAJOR:
			ret = Ab_MAJOR_KEY;
			ret.notes[0] = Ab_NOTE;	
			ret.notes[1] = Bb_NOTE;	
			ret.notes[2] = C_NOTE;	
			ret.notes[3] = Db_NOTE;	
			ret.notes[4] = Eb_NOTE;	
			ret.notes[5] = F_NOTE;	
			ret.notes[6] = G_NOTE;
			break;
		case Eb_MAJOR:
			ret = Eb_MAJOR_KEY;
			ret.notes[0] = Eb_NOTE;	
			ret.notes[1] = F_NOTE;	
			ret.notes[2] = G_NOTE;	
			ret.notes[3] = Ab_NOTE;	
			ret.notes[4] = Bb_NOTE;	
			ret.notes[5] = C_NOTE;	
			ret.notes[6] = D_NOTE;
			break;
		case Bb_MAJOR:
			ret = Bb_MAJOR_KEY;
			ret.notes[0] = Bb_NOTE;	
			ret.notes[1] = C_NOTE;	
			ret.notes[2] = D_NOTE;	
			ret.notes[3] = Eb_NOTE;	
			ret.notes[4] = F_NOTE;	
			ret.notes[5] = G_NOTE;	
			ret.notes[6] = A_NOTE;
			break;
		case F_MAJOR:
			ret = F_MAJOR_KEY;
			ret.notes[0] = F_NOTE;	
			ret.notes[1] = G_NOTE;	
			ret.notes[2] = A_NOTE;	
			ret.notes[3] = Bb_NOTE;	
			ret.notes[4] = C_NOTE;	
			ret.notes[5] = D_NOTE;	
			ret.notes[6] = E_NOTE;
			break;
		case C_MAJOR:
			ret = C_MAJOR_KEY;
			ret.notes[0] = C_NOTE;	
			ret.notes[1] = D_NOTE;	
			ret.notes[2] = E_NOTE;	
			ret.notes[3] = F_NOTE;	
			ret.notes[4] = G_NOTE;	
			ret.notes[5] = A_NOTE;	
			ret.notes[6] = B_NOTE;
			break;
		case G_MAJOR:
			ret = G_MAJOR_KEY;
			ret.notes[0] = G_NOTE;	
			ret.notes[1] = A_NOTE;	
			ret.notes[2] = B_NOTE;	
			ret.notes[3] = C_NOTE;	
			ret.notes[4] = D_NOTE;	
			ret.notes[5] = E_NOTE;	
			ret.notes[6] = FS_NOTE;
			break;
		case D_MAJOR:
			ret = D_MAJOR_KEY;
			ret.notes[0] = D_NOTE;	
			ret.notes[1] = E_NOTE;	
			ret.notes[2] = FS_NOTE;	
			ret.notes[3] = G_NOTE;	
			ret.notes[4] = A_NOTE;	
			ret.notes[5] = B_NOTE;	
			ret.notes[6] = CS_NOTE;
			break;
		case A_MAJOR:
			ret = A_MAJOR_KEY;
			ret.notes[0] = A_NOTE;	
			ret.notes[1] = B_NOTE;	
			ret.notes[2] = CS_NOTE;	
			ret.notes[3] = D_NOTE;	
			ret.notes[4] = E_NOTE;	
			ret.notes[5] = FS_NOTE;	
			ret.notes[6] = GS_NOTE;
			break;
		case E_MAJOR:
			ret = E_MAJOR_KEY;
			ret.notes[0] = E_NOTE;	
			ret.notes[1] = FS_NOTE;	
			ret.notes[2] = GS_NOTE;	
			ret.notes[3] = A_NOTE;	
			ret.notes[4] = B_NOTE;	
			ret.notes[5] = CS_NOTE;	
			ret.notes[6] = DS_NOTE;
			break;
		case B_MAJOR:
			ret = B_MAJOR_KEY;
			ret.notes[0] = B_NOTE;	
			ret.notes[1] = CS_NOTE;	
			ret.notes[2] = DS_NOTE;	
			ret.notes[3] = E_NOTE;	
			ret.notes[4] = FS_NOTE;	
			ret.notes[5] = GS_NOTE;	
			ret.notes[6] = AS_NOTE;
			break;
		case FS_MAJOR:
			ret = FS_MAJOR_KEY;
			ret.notes[0] = FS_NOTE;	
			ret.notes[1] = GS_NOTE;	
			ret.notes[2] = AS_NOTE;	
			ret.notes[3] = B_NOTE;	
			ret.notes[4] = CS_NOTE;	
			ret.notes[5] = DS_NOTE;	
			ret.notes[6] = ES_NOTE;
			break;
		case CS_MAJOR:
			ret = CS_MAJOR_KEY;
			ret.notes[0] = CS_NOTE;	
			ret.notes[1] = DS_NOTE;	
			ret.notes[2] = ES_NOTE;	
			ret.notes[3] = FS_NOTE;	
			ret.notes[4] = GS_NOTE;	
			ret.notes[5] = AS_NOTE;	
			ret.notes[6] = BS_NOTE;
			break;	
	}	
	return ret;
}
/*
int main(void){
	
	int i,j;
	for(i=Cb_MAJOR;i<=CS_MAJOR;i++){
		key my_key = getKeyInfo(i);
		printf("Key of %10s:\t",my_key.name);
		for(j=0;j<NUMBER_OF_NOTE_LETTERS;j++){
			printf("%s\t", my_key.notes[j].name);
		}
		printf("\n");
	}
	return 0;
}*/
